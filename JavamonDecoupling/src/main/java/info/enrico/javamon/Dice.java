/**
 * 
 */
package info.enrico.javamon;

import java.util.Random;

/**
 * Yeah nigga, the allmighty dice is back
 * @author Pello Altadill
 * @greetz Simon Dice
 */
public class Dice {
	private int sides;
	private Random random = new Random();
	
	/**
	 * default constructor
	 * creates a 6D dice
	 */
	public Dice () {
		this.sides = 6;
	}
	
	/**
	 * Constructor with side number as argument
	 * @param sides
	 */
	public Dice (int sides) {
		this.sides = sides;
	}
	
	/**
	 * rolls the dice and return result
	 * @return
	 */
	public int roll () {
		return random.nextInt(sides) + 1;
	}

}
