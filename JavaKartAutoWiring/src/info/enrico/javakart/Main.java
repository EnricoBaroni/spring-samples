package info.enrico.javakart;


import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;


/**
 * Inits JavaKart race
 * @author Pello Altadill
 * @greetz for the blue mug
 */
public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		

        ApplicationContext context = new ClassPathXmlApplicationContext("javakart.xml"); //Para poder aplicar los spring		 
		Race race = (Race) context.getBean("race"); //Para poder crear instancia de combat
           
		System.out.println(race.toString());
        race.run();
        System.out.println(race.showResult());
	}

}
